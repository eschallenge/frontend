export class GitHubRepo {
  id:                 number;
  name:               string;
  url:                string;
  branches_url:       string;
  default_branch:     string;
  created_at:         any;
  last_commit_date:   any;
}
