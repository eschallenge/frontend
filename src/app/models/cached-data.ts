import {GitHubRepo} from './git-hub-repo';

export class CachedData {
  cached_at:      string;
  repos:          GitHubRepo[];
}
