export interface IOrderByPipe {
  isDesc:     boolean;
  column:     string;
}
