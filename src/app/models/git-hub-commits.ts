export class GitHubCommits {
  commit:     Commit;
}

export class Commit {
  commit?:    Commit;
  author:     Author;
}

export class Author {
  date:       string;
}
