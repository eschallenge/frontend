import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OrderByPipe} from '../../pipes/order-by.pipe';
import {SearchFilterPipe} from '../../pipes/search-filter.pipe';

@NgModule({
  declarations: [
    OrderByPipe,
    SearchFilterPipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    OrderByPipe,
    SearchFilterPipe
  ]
})
export class SharedModule { }
