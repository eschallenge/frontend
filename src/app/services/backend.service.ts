import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {GitHubCommits} from '../models/git-hub-commits';
import {catchError} from 'rxjs/operators';
import {GitHubRepo} from '../models/git-hub-repo';
import {CachedData} from '../models/cached-data';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  gitHubAPIURL  = 'https://api.github.com';
  serverAPIURL  = 'http://ecosafe.test/api/repos';

  constructor(private http: HttpClient) { }

  /**
   * Get all the repos from githubtraining
   */
  getAllRepos(): Observable<GitHubRepo[]> {
    return this.http.get<GitHubRepo[]>(`${this.gitHubAPIURL}/users/githubtraining/repos`)
      .pipe(catchError(this.handleServerError));
  }

  /**
   * Get all commits for a single repo
   */
  getCommitsForRepo(url: string): Observable<GitHubCommits> {
    return this.http.get<GitHubCommits>(url)
      .pipe(catchError(this.handleServerError));
  }

  /**
   * Save the 10 most recent repos in the database.
   */
  cacheData(data: GitHubRepo[]) {
    return this.http.post(`${this.serverAPIURL}/cache`, data)
      .pipe(catchError(this.handleServerError));
  }

  /**
   * Fetch the 10 most repos from the database
   */
  getCachedData(): Observable<CachedData[]> {
    return this.http.get<CachedData[]>(`${this.serverAPIURL}/`)
      .pipe(catchError(this.handleServerError));
  }

  handleServerError(error: any) {
    console.error('An error occurred', error);
    return throwError(error);
  }

}
