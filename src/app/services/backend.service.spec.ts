import {getTestBed, TestBed} from '@angular/core/testing';

import { BackendService } from './backend.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {GitHubRepo} from '../models/git-hub-repo';
import {GitHubCommits} from '../models/git-hub-commits';
import {CachedData} from '../models/cached-data';
import {HttpErrorResponse} from '@angular/common/http';

describe('BackendService', () => {

  // Only inject the service once
  let service: BackendService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BackendService]
    });

    httpMock = getTestBed().get(HttpTestingController);
    service = getTestBed().get(BackendService);
  });

  // Ensure no requests that are outstanding so there are no
  // state issues going into another test
  afterEach(() => {
    httpMock.verify();
  });

  it('is created', () => {
    expect(BackendService).toBeTruthy();
  });

  it('should retrieve repos from GitHub API', () => {
    const dummyRepos: GitHubRepo[] = [
      {
        id: 1,
        name: 'advanced-git',
        default_branch: 'master',
        created_at: '2018-01-24T13:40:11Z',
        last_commit_date: '2018-01-24T13:40:11Z',
        url: 'https://api.github.com/repos/githubtraining/advanced-git',
        branches_url: 'https://api.github.com/repos/githubtraining/AlienInvasion/branches{/branch}',
      }
    ];
    service.getAllRepos().subscribe(repos => {
      expect(repos.length).toBe(1);
      expect(repos).toEqual(dummyRepos);
    });

    // Expect only 1 request made to the service
    const request = httpMock.expectOne(`${service.gitHubAPIURL}/users/githubtraining/repos`);

    // Except method of HTTP call to be GET
    expect(request.request.method).toBe('GET');

    // Allow the get repos to use the dummyRepos to fire off the request
    request.flush(dummyRepos);

  });

  it('should retrieve repos from the local Backend', () => {
    const dummyRepos: CachedData[] = [

      {
        cached_at: '2018-01-24T13:40:11Z',
        repos: [
          {
            id: 1,
            name: 'advanced-git',
            default_branch: 'master',
            created_at: '2018-01-24T13:40:11Z',
            last_commit_date: '2018-01-24T13:40:11Z',
            url: 'https://api.github.com/repos/githubtraining/advanced-git',
            branches_url: 'https://api.github.com/repos/githubtraining/AlienInvasion/branches{/branch}',
          }
        ]
      }
    ];
    service.getCachedData().subscribe(repos => {
      expect(repos.length).toBe(1);
      expect(repos).toEqual(dummyRepos);
    });

    // Expect only 1 request made to the service
    const request = httpMock.expectOne(`${service.serverAPIURL}/`);

    // Except method of HTTP call to be GET
    expect(request.request.method).toBe('GET');

    // Allow the get repos to use the dummyRepos to fire off the request
    request.flush(dummyRepos);

  });

  it('should retrieve the commit for the repo', () => {
    const branchesURL = 'https://api.github.com/repos/training/master';
    const dummyCommit: any = {
      commit: {
        commit: {
          author: {
            date: '2018-01-24T13:40:11Z'
          }
        }
      }
    };
    service.getCommitsForRepo(branchesURL).subscribe(repos => {
      expect(repos).toEqual(dummyCommit);
    });

    // Expect only 1 request made to the service
    const request = httpMock.expectOne(branchesURL);

    // Except method of HTTP call to be GET
    expect(request.request.method).toBe('GET');

    // Allow the get repos to use the dummyRepos to fire off the request
    request.flush(dummyCommit);

  });

  it('should handleErrorObservable', () =>
  {
    const errorMessage = 'An error has occurred';

    spyOn(service, 'handleServerError').and.callThrough();

    service.getCachedData().subscribe(
      data => fail(errorMessage),
      (error: HttpErrorResponse) =>
      {
        expect(service.handleServerError).toHaveBeenCalled(); // check if executed
        expect(error.status).toEqual(400, 'status');
        expect(error.error).toEqual(errorMessage, 'message');
      });

    const request = httpMock.expectOne(`${service.serverAPIURL}/`);
    // Respond with mock error
    request.flush(errorMessage, {status: 400, statusText: 'Not Found'});

  });
});
