import {async, ComponentFixture, TestBed, tick} from '@angular/core/testing';

import { TableComponent } from './table.component';
import {CUSTOM_ELEMENTS_SCHEMA, DebugElement} from '@angular/core';
import {By} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../modules/shared/shared.module';


describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;
  let debugElement: DebugElement;
  let element: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        SharedModule
      ],
      declarations: [ TableComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    debugElement = fixture.debugElement.query(By.css('.results-table'));
    element  = debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a table to display the pastes', () => {
    expect(element.innerHTML).toContain('thead');
    expect(element.innerHTML).toContain('tbody');
  });

  it('should have text search input', () => {
    expect(element.innerHTML).toContain('input');
  });

  it('onReset should be called when the reset button is clicked', () => {
    spyOn(component, 'onReset');
    const resetBtn = fixture.debugElement.query(By.css('.btn-reset')).nativeElement;
    resetBtn.click();
    fixture.whenStable().then(() => {
      expect(component.onReset).toHaveBeenCalled();
    });
  });

  it('onReset should be called', () => {
    spyOn(component, 'onReset');
    component.onReset();
    expect(component.onReset).toHaveBeenCalled();
  });

});
