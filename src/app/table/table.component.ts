import {Component, Input, OnInit} from '@angular/core';
import {GitHubRepo} from '../models/git-hub-repo';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  @Input() data:      GitHubRepo[]; // Data to be shown in table.
  @Input() keys:      String[]; // We only want to show a few columns so we filter out the ones we don't want to show
  @Input() caption:   String; // Title that appears above the table
  @Input() cachedAt:  String; // Date data was cached at

  isDescSort      = false; // Track if data should be sorted in ascending or descending order
  columnToSort    = ''; // Tracks which column we are sorting by (Create/Commit date)
  searchKeyword   =  ''; // Keyword to filter search results

  constructor() { }

  ngOnInit() {
  }

  onSort = (column) => {
    this.columnToSort = column; // Set the column you want to sort.
    this.isDescSort = !this.isDescSort; // Change the direction of sort;
  }

  // Reset to the initial state
  onReset = () => {
    this.columnToSort = '';
    this.isDescSort = false;
    this.searchKeyword = '';
  }
}
