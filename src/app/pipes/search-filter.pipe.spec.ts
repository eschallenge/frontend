import { SearchFilterPipe } from './search-filter.pipe';
import {GitHubRepo} from '../models/git-hub-repo';

describe('SearchFilterPipe', () => {
  let pipe: SearchFilterPipe;

  beforeEach(() => {
    pipe = new SearchFilterPipe();
  });

  const dummyRepos: GitHubRepo[] = [
    {
      id: 1,
      name: 'advanced-git',
      default_branch: 'master',
      created_at: '2018-01-24T13:40:11Z',
      last_commit_date: '2018-01-24T13:40:11Z',
      url: 'https://api.github.com/repos/githubtraining/advanced-git',
      branches_url: 'https://api.github.com/repos/githubtraining/AlienInvasion/branches{/branch}',
    },
    {
      id: 2,
      name: 'git tutorials',
      default_branch: 'master',
      created_at: '2018-01-24T13:40:11Z',
      last_commit_date: '2018-01-24T13:40:11Z',
      url: 'https://api.github.com/repos/githubtraining/advanced-git',
      branches_url: 'https://api.github.com/repos/githubtraining/AlienInvasion/branches{/branch}',
    },
    {
      id: 3,
      name: 'git-circle',
      default_branch: 'master',
      created_at: '2018-01-24T13:40:11Z',
      last_commit_date: '2018-01-24T13:40:11Z',
      url: 'https://api.github.com/repos/githubtraining/advanced-git',
      branches_url: 'https://api.github.com/repos/githubtraining/AlienInvasion/branches{/branch}',
    },
    {
      id: 4,
      name: 'UI Library',
      default_branch: 'master',
      created_at: '2018-01-24T13:40:11Z',
      last_commit_date: '2018-01-24T13:40:11Z',
      url: 'https://api.github.com/repos/githubtraining/advanced-git',
      branches_url: 'https://api.github.com/repos/githubtraining/AlienInvasion/branches{/branch}',
    },
    {
      id: 5,
      name: 'Angular Tutorials',
      default_branch: 'master',
      created_at: '2018-01-24T13:40:11Z',
      last_commit_date: '2018-01-24T13:40:11Z',
      url: 'https://api.github.com/repos/githubtraining/advanced-git',
      branches_url: 'https://api.github.com/repos/githubtraining/AlienInvasion/branches{/branch}',
    }
  ];

  it('providing no keyword should return the original array', () => {
    expect(pipe.transform(dummyRepos, ''))
      .toBe(dummyRepos);
  });

  it('the keyword "git" should return 3 results', () => {
    const keyword = 'git';
    const clone = [...dummyRepos.filter(x => x.name.toLowerCase().includes(keyword))];
    expect(pipe.transform(dummyRepos, keyword)).toEqual(clone);
  });

  it('the keyword "GIT" in all caps should return 3 results', () => {
    const clone = [...dummyRepos.filter(x => x.name.includes('git'))];
    expect(pipe.transform(dummyRepos, 'GIT')).toEqual(clone);
  });

  it('the keyword "React Native" should return an empty array', () => {
    const expectedResult = [];
    expect(pipe.transform(dummyRepos, 'React Native')).toEqual(expectedResult);
  });

  it('the keyword "UI" should return 1 result', () => {
    expect(pipe.transform(dummyRepos, 'UI').length).toEqual(1);
  });

});
