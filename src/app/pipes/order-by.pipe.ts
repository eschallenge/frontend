import { Pipe, PipeTransform } from '@angular/core';
import {GitHubRepo} from '../models/git-hub-repo';
import {IOrderByPipe} from '../models/i-order-by-pipe';
import {throwError} from 'rxjs';

@Pipe({
  name: 'orderBy'
})

/**
 * This pipe is used to sort the created at and last commit columns.
 */
export class OrderByPipe implements PipeTransform {

  transform(repos: GitHubRepo[], args?: IOrderByPipe): GitHubRepo[] {
    if ( !args.column)
    {
      return repos;
    }
    return repos.sort((a, b) => {
      if (args.isDesc)
      {
        return a[args.column] > b[args.column] ? -1 : 1;
      }
      else
      {
        return a[args.column] > b[args.column] ? 1 : -1;
      }
    });
  }

}
