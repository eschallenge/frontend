import { Pipe, PipeTransform } from '@angular/core';
import {GitHubRepo} from '../models/git-hub-repo';

@Pipe({
  name: 'searchFilter'
})

/**
 * This pipe is used filter the repos by name.
 */

export class SearchFilterPipe implements PipeTransform {

  transform(repos: GitHubRepo[], keyword: string): GitHubRepo[] {
    if (!repos)
    {
      return [];
    }
    if (!keyword)
    {
      return repos;
    }

    keyword = keyword.toLowerCase();

    return repos.filter(repo => {
      return repo.name.toLowerCase().includes(keyword);
    });
  }
}
