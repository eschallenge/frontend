import { OrderByPipe } from './order-by.pipe';
import {GitHubRepo} from '../models/git-hub-repo';
import {IOrderByPipe} from '../models/i-order-by-pipe';

describe('OrderByPipe', () => {

  let pipe: OrderByPipe;

  beforeEach(() => {
    pipe =  new OrderByPipe();
  });

  const dummyRepos: GitHubRepo[] = [
    {
      id: 1,
      created_at: '2014-01-24T13:40:11Z',
      last_commit_date: '2015-01-24T13:40:11Z',
      name: '', default_branch: '', url: '', branches_url: '',
    },
    {
      id: 2,
      created_at: '2015-01-24T13:40:11Z',
      last_commit_date: '2016-01-24T13:40:11Z',
      name: '', default_branch: '', url: '', branches_url: '',
    },
    {
      id: 3,
      created_at: '2016-01-24T13:40:11Z',
      last_commit_date: '2017-01-24T13:40:11Z',
      name: '', default_branch: '', url: '', branches_url: '',
    },
  ];

  it('create an instance', () => {
     expect(pipe).toBeTruthy();
  });

  it('should return in the same order as the original data (sort by created_at date)', () => {
    const args: IOrderByPipe = {column: 'created_at', isDesc: false};
    expect(pipe.transform(dummyRepos, args)).toEqual(dummyRepos);
  });

  it('should return in the same order as the original data (sort by last commit date)', () => {
    const args: IOrderByPipe = {column: 'last_commit_Date', isDesc: false};
    expect(pipe.transform(dummyRepos, args)).toEqual(dummyRepos);
  });

  it('should return in descending order (sort by created_at date)', () => {
    const args: IOrderByPipe = {column: 'created_at', isDesc: true};
    const expectedOutput: GitHubRepo[] = [
      {
        id: 3,
        created_at: '2016-01-24T13:40:11Z',
        last_commit_date: '2017-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
      {
        id: 2,
        created_at: '2015-01-24T13:40:11Z',
        last_commit_date: '2016-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
      {
        id: 1,
        created_at: '2014-01-24T13:40:11Z',
        last_commit_date: '2015-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
    ];
    expect(pipe.transform(dummyRepos, args)).toEqual(expectedOutput);
  });

  it('should return in descending order (sort by last commit date)', () => {
    const args: IOrderByPipe = {column: 'last_commit_date', isDesc: true};
    const expectedOutput: GitHubRepo[] = [
      {
        id: 3,
        created_at: '2016-01-24T13:40:11Z',
        last_commit_date: '2017-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
      {
        id: 2,
        created_at: '2015-01-24T13:40:11Z',
        last_commit_date: '2016-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
      {
        id: 1,
        created_at: '2014-01-24T13:40:11Z',
        last_commit_date: '2015-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
    ];
    expect(pipe.transform(dummyRepos, args)).toEqual(expectedOutput);
  });

  it('', () => {
    const args: IOrderByPipe = {column: 'created_at', isDesc: true};
    const testCaseRepo: GitHubRepo[] = [
      {
        id: 1,
        created_at: '2000-01-24T13:40:11Z',
        last_commit_date: '2015-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
      {
        id: 2,
        created_at: '2018-01-24T13:40:11Z',
        last_commit_date: '2016-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
      {
        id: 3,
        created_at: '2014-01-24T13:40:11Z',
        last_commit_date: '2017-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
    ];
    const expectedOutput: GitHubRepo[] = [
      {
        id: 2,
        created_at: '2018-01-24T13:40:11Z',
        last_commit_date: '2016-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
      {
        id: 3,
        created_at: '2014-01-24T13:40:11Z',
        last_commit_date: '2017-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
      {
        id: 1,
        created_at: '2000-01-24T13:40:11Z',
        last_commit_date: '2015-01-24T13:40:11Z',
        name: '', default_branch: '', url: '', branches_url: '',
      },
    ];
    expect(pipe.transform(testCaseRepo, args)).toEqual(expectedOutput);
  });

});
