import { Component, OnInit } from '@angular/core';
import {BackendService} from '../services/backend.service';
import {GitHubRepo} from '../models/git-hub-repo';
import {finalize, switchMap} from 'rxjs/operators';
import {forkJoin} from 'rxjs';
import { map } from 'rxjs/operators';
import {OrderByPipe} from '../pipes/order-by.pipe';
import {IOrderByPipe} from '../models/i-order-by-pipe';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [OrderByPipe]
})
export class HomeComponent implements OnInit {

  gitData: GitHubRepo[];

  loading = true; // Displays spinner until data is loaded
  viewingCached = false; //
  cachedAtDate = '';
  gitKeys: string[] = ['name', 'created_at', 'last_commit_date']; // these keys are passed to the table component so that only these 3 columns are displayed


  constructor(private API: BackendService, private orderByPipe: OrderByPipe, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.viewingCached = this.activatedRoute.snapshot.data['cached']; // check if we are viewing cached data
    if (this.viewingCached)
    {
      this.getCachedRepos();
    }
    else
    {
      this.getGitRepos();
    }
  }

  getGitRepos = () => {
    // Get all github repos
    this.API.getAllRepos()
      .pipe(switchMap((repos) => {
        return forkJoin(repos.map(repo => {
          // get the default branch & url for the branches
          repo.branches_url = repo.branches_url.replace('{/branch}', `/${repo.default_branch}`);
          // get the latest commit date
          return this.API.getCommitsForRepo(repo.branches_url)
            .pipe(map(commit => {
              repo.last_commit_date = commit.commit.commit.author.date;
              return repo;
            }));
        })).pipe(finalize( () => {
          this.loading = false;
          this.cacheRepos();
        }));
      })).subscribe(repos => {
        this.gitData = repos;
    });
  }

  cacheRepos = () => {
    const args: IOrderByPipe = {isDesc: true, column: 'created_at'};
    const sortedData: GitHubRepo[] = this.orderByPipe.transform(this.gitData, args).slice(0, 10); // Get data sorted by date repo is created, and take first 10
    this.API.cacheData(sortedData).subscribe();
  }

  getCachedRepos = () => {
    this.API.getCachedData().subscribe(data => {
      if (data && data.length)
      {
        this.cachedAtDate = data[0].cached_at;
        this.gitData = data[0].repos;
      }
      this.loading = false;

    });
  }
}

