import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';

import { HomeComponent } from './home.component';
import {TableComponent} from '../table/table.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {LoadingSpinnerComponent} from '../loading-spinner/loading-spinner.component';
import {RouterTestingModule} from '@angular/router/testing';
import {BackendService} from '../services/backend.service';
import {GitHubRepo} from '../models/git-hub-repo';
import { from } from 'rxjs';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '../modules/shared/shared.module';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let service: BackendService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        SharedModule,
      ],
      declarations: [
        HomeComponent,
        TableComponent,
        LoadingSpinnerComponent,
      ],
      providers: [
        BackendService,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    service = TestBed.get(BackendService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get the git repos from the GitHub API', fakeAsync(() => {

    const dummyRepos: GitHubRepo[] = [
      {
        id: 1,
        name: 'advanced-git',
        default_branch: 'master',
        created_at: '2018-01-24T13:40:11Z',
        last_commit_date: '2018-01-24T13:40:11Z',
        url: 'https://api.github.com/repos/githubtraining/advanced-git',
        branches_url: 'https://api.github.com/repos/githubtraining/AlienInvasion/branches{/branch}',
      }
    ];

    const dummyCommit = {
      commit: {
        commit: {
          author: {
            date: ''
          }
        }
      }
    };

    spyOn(service, 'getAllRepos').and.callFake(() => {
      return from([dummyRepos]);
    });

    spyOn(service, 'getCommitsForRepo').and.callFake(() => {
      return from([dummyCommit]);
    });

    component.ngOnInit();
    tick();
    component.getGitRepos();
    expect(component.gitData).toEqual(dummyRepos);
    expect(service.getAllRepos).toHaveBeenCalled();
    expect(service.getCommitsForRepo).toHaveBeenCalled();
  }));

  it('should get the cached repos from the backend', fakeAsync(() => {

    const dummyRepos: GitHubRepo[] = [
      {
        id: 1,
        name: 'advanced-git',
        default_branch: 'master',
        created_at: '2018-01-24T13:40:11Z',
        last_commit_date: '2018-01-24T13:40:11Z',
        url: 'https://api.github.com/repos/githubtraining/advanced-git',
        branches_url: 'https://api.github.com/repos/githubtraining/AlienInvasion/branches{/branch}',
      }
    ];

    spyOn(service, 'getCachedData').and.callFake(() => {
      return from([dummyRepos]);
    });

    component.ngOnInit();
    component.getCachedRepos();
    expect(service.getCachedData).toHaveBeenCalled();
  }));

});
